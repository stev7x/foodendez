<!-- Header -->
@include('app.partials.header')

<!-- Content -->
@yield('content')

<!-- Footer -->
@include('app.partials.footer')
