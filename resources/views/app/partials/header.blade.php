<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ env('APP_NAME') }} | {{ $title }}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('vegefood/images/logo-ico.png') }}">
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('vegefood/css/open-iconic-bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/animate.css') }}">

        <link rel="stylesheet" href="{{ asset('vegefood/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/magnific-popup.css') }}">

        <link rel="stylesheet" href="{{ asset('vegefood/css/aos.css') }}">

        <link rel="stylesheet" href="{{ asset('vegefood/css/ionicons.min.css') }}">

        <link rel="stylesheet" href="{{ asset('vegefood/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/jquery.timepicker.css') }}">


        <link rel="stylesheet" href="{{ asset('vegefood/css/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('vegefood/css/style.css') }}">
    </head>
    <body class="goto-here">
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}"><img class="img-fluid" src="{{ asset('vegefood/images/logo-black.png') }}" alt="Logo Foodendez"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>

                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item {{ $page == 'Home' ? 'active' : '' }}">
                            <a href="{{ route('home') }}" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item {{ $page == 'About' ? 'active' : '' }}">
                            <a href="{{ route('about') }}" class="nav-link">About</a>
                        </li>
                        <li class="nav-item {{ $page == 'Contact' ? 'active' : '' }}">
                            <a href="{{ route('contact') }}" class="nav-link">Contact</a>
                        </li>
                        <li class="nav-item {{ $page == 'Cart' ? 'active' : '' }} cta cta-colored">
                            <a href="{{ route('cart') }}" class="nav-link">
                                <span class="icon-shopping_cart"></span>[<span id="chart_counter">0</span>]
                            </a>
                        </li>
                        @if (Route::has('login'))
                            @auth
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('login') }}" class="nav-link">Login</a>
                                </li>

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a href="{{ route('register') }}" class="nav-link">Register</a>
                                    </li>
                                @endif
                            @endauth
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->
