        
        <div id="wws-layout-4" class="wws-popup-container wws-popup-container--position">
            <!-- .Popup footer -->
            <div class="wws-popup__footer">
                <!-- Popup open button -->
                <div class="wws-popup__open-btn wws-popup__send-btn wws-shadow wws--text-color wws--bg-color">
                <a target="_blank" href="https://api.whatsapp.com/send?phone=6285795130165&text=Halo%20Foodendez%20saya%20ingin%20bertanya."><span class="icon-whatsapp wws-popup__open-icon"></span></a>
                </div>
                <!-- .Popup open button -->
            </div>
            <!-- Popup footer -->

        </div>
        
        <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
            <div class="container py-4">
                <div class="row d-flex justify-content-center py-5">
                    <div class="col-md-6">
                        <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                        <span>Get e-mail updates about our latest shops and special offers</span>
                    </div>
                    <div class="col-md-6 d-flex align-items-center">
                        <form action="#" onsubmit="subscribe(); return false;" class="subscribe-form">
                            <div class="form-group d-flex">
                                <input type="text" class="form-control" id="subscribe_email" placeholder="Enter email address">
                                <input type="submit" value="Subscribe" class="submit px-3">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <footer class="ftco-footer ftco-section">
            <div class="container">
                <div class="row">
                    <div class="mouse">
                        <a href="#" class="mouse-icon">
                            <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
                        </a>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4">
                            <h2 class="ftco-heading-2">Foodendez</h2>
                            <ul class="ftco-footer-social list-unstyled">
                                <li class="ftco-animate"><a href="{{ config('company_profile.ig.url') }}"><span class="icon-instagram"></span></a></li>
                                <li class="ftco-animate"><a href="https://api.whatsapp.com/send?phone=6285795130165&text=Halo%20Foodendez%20saya%20ingin%20bertanya."><span class="icon-whatsapp"></span></a></li>
                                <li class="ftco-animate"><a href="mailto:foodendez@gmail.com"><span class="icon-envelope"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4 ml-md-5">
                            <h2 class="ftco-heading-2">Menu</h2>
                            <ul class="list-unstyled">
                                <li><a href="{{ route('home') }}" class="py-2 d-block">Home</a></li>
                                <li><a href="{{ route('about') }}" class="py-2 d-block">About</a></li>
                                <li><a href="{{ route('contact') }}" class="py-2 d-block">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4">
                            <h2 class="ftco-heading-2">Have a Questions?</h2>
                            <div class="block-23 mb-3">
                                <ul>
                                    <li><a target="_blank" href="https://www.google.com/maps/place/6%C2%B056'40.5%22S+107%C2%B044'19.2%22E/@-6.9445856,107.7364654,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d-6.9445856!4d107.7386541?hl=en"><span class="icon icon-map-marker"></span><span class="text">{{ config('company_profile.alamat') }}</span></a></li>
                                    <li><a href="tel:+6285795130165"><span class="icon icon-phone"></span><span class="text">{{ config('company_profile.phone') }}</span></a></li>
                                    <li><a href="mailto:foodendez@gmail.com"><span class="icon icon-envelope"></span><span class="text">{{ config('company_profile.email') }}</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">

                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | made with <i class="icon-heart color-danger" aria-hidden="true"></i> <a href="#" target="_blank">Foodendez</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
        
        <script src="{{ asset('vegefood/js/jquery.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery-migrate-3.0.1.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/popper.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery.stellar.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/aos.js') }}"></script>
        <script src="{{ asset('vegefood/js/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('vegefood/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('vegefood/js/scrollax.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBkW3l4xJIvrDcLskksqmaJ8g1oZZBKLQ&sensor=false"></script>
        <script src="{{ asset('vegefood/js/google-map.js') }}"></script>
        <script src="{{ asset('vegefood/js/main.js') }}"></script>
        <script src="{{ asset('js/'.$js) }}"></script>
    </body>
</html>
