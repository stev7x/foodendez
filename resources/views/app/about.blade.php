@extends('app.partials.main')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{ asset('images/bg/bg-2.jpg') }}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-0 bread">About us</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-no-pb bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ asset('images/about.jpg') }});">
                    <a href="https://www.youtube.com/watch?v=yZMEP34hOtM" class="icon popup-vimeo d-flex justify-content-center align-items-center">
                        <span class="icon-play"></span>
                    </a>
                </div>
                <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
                    <div class="heading-section-bold mb-4 mt-md-5">
                        <div class="ml-md-0">
                            <h2 class="mb-4">Welcome to {{ config('company_profile.nama') }} an eCommerce website</h2>
                        </div>
                    </div>
                    <div class="pb-md-5">
                        <p>{{ config('company_profile.deskripsi') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
