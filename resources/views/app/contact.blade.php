@extends('app.partials.main')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{ asset('images/bg1.jpg') }}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-0 bread">Contact us</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <div class="row d-flex mb-5 contact-info">
                <div class="w-100"></div>
                <div class="col-md-4 d-flex">
                    <div class="info bg-white p-4">
                        <p><span>Address:</span> <a target="_blank" href="https://www.google.com/maps/place/6%C2%B056'40.5%22S+107%C2%B044'19.2%22E/@-6.9445856,107.7364654,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d-6.9445856!4d107.7386541?hl=en">{{ config('company_profile.alamat') }}</a></p>
                    </div>
                </div>
                <div class="col-md-4 d-flex">
                    <div class="info bg-white p-4">
                        <p><span>Phone:</span> <a href="tel:+6285795130165">{{ config('company_profile.phone') }}</a></p>
                    </div>
                </div>
                <div class="col-md-4 d-flex">
                    <div class="info bg-white p-4">
                        <p><span>Email:</span> <a href="mailto:foodendez@gmail.com">{{ config('company_profile.email') }}</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row block-9">
                <div class="col-md-6 order-md-last d-flex">
                    <form method="post" action="{{url('sendemail/send')}}" class="bg-white p-5 contact-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" class="form-control" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea id="" cols="30" rows="7" name="message" class="form-control" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="send" value="Send Message" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>

                <div class="col-md-6 d-flex">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="564" height="564" id="gmap_canvas" src="https://maps.google.com/maps?q=foodendez&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="10px" marginwidth="10px"></iframe>
                        </div>
                        <style>.mapouter{position:relative;text-align:right;height:100%;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:100%;width:100%;}</style>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
