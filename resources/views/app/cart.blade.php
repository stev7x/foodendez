@extends('app.partials.main')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{ asset('images/bg/bg-1.jpg') }}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-0 bread">My Cart</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>

    <section class="ftco-section ftco-cart">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Product name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody id="product_list">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('create_order') }}">
                @csrf
                <div class="row justify-content-end">
                    <div class="col-lg-8 mt-5 cart-wrap ftco-animate">
                        <div class="cart-total mb-3">
                            <h3>Orders Form</h3>
                            <div class="info">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" type="text" name="name" class="form-control text-left px-3" placeholder="Name" value="{{ @Auth::user()->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" name="email" class="form-control text-left px-3" placeholder="Email" value="{{ @Auth::user()->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input id="phone" type="text" name="phone" class="form-control text-left px-3" placeholder="Phone" value="{{ @Auth::user()->phone }}">
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea id="address" name="address" class="form-control text-left px-3 p-3" placeholder="Address">{{ @Auth::user()->address }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="pos_code">Zip/Postal Code</label>
                                    <input id="pos_code" type="text" name="pos_code" class="form-control text-left px-3" placeholder="Zip/Postal Code">
                                </div>
                                <div class="form-group">
                                    <input type="submit" onclick="checkout();" class="btn btn-primary py-3 px-4" value="Checkout">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                        <div class="cart-total mb-3">
                            <h3>Cart Totals</h3>
                            <p class="d-flex">
                                <span>Subtotal</span>
                                <span id="v_subtotal">20.60</span>
                            </p>
                            <p class="d-flex">
                                <span>Delivery</span>
                                <span id="v_delivery">$0.00</span>
                            </p>
                            <p class="d-flex">
                                <span>Discount</span>
                                <span id="v_discount">$3.00</span>
                            </p>
                            <hr>
                            <p class="d-flex total-price">
                                <span>Total</span>
                                <span id="v_total">$17.60</span>
                            </p>
                        </div>
                    </div>

                    <input type="hidden" id="subtotal" name="subtotal" value="">
                    <input type="hidden" id="delivery" name="delivery" value="">
                    <input type="hidden" id="discount" name="discount" value="">
                    <input type="hidden" id="total" name="total" value="">
                    <input type="hidden" id="transaction_code" name="transaction_code" value="">

                    <div id="item_product"></div>
                </div>
            </form>
        </div>
    </section>
@endsection
