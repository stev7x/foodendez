@extends('app.partials.main')

@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 ftco-animate">
                    <a href="#" class="image-popup"><img src="{{ asset('images/products/'.$product[$product_id]['display']) }}" class="img-fluid" alt="Colorlib Template"></a>
                </div>
                <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                    <h3>{{ $product[$product_id]['name'] }}</h3>
                    <div class="rating d-flex">
                        <p class="text-left">
                            <a href="#" class="mr-2" style="color: #000;">{{ $stock[$product_id]['stock_out'] }} <span style="color: #bbb;"> Sold</span></a>
                        </p>
                    </div>
                    <p class="price"><span>Rp {{ number_format($product[$product_id]['price']) }}</span></p>
                    <p>{{ $product[$product_id]['description'] }}</p>
                    <div class="row mt-4">
                        <div class="w-100"></div>
                        <div class="input-group col-md-6 d-flex mb-3">
                            <span class="input-group-btn mr-2">
                                <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                                    <i class="ion-ios-remove"></i>
                                </button>
                            </span>
                            <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                            <span class="input-group-btn ml-2">
                                <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                                    <i class="ion-ios-add"></i>
                                </button>
                            </span>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <p style="color: #000;">{{ $stock[$product_id]['curr_stock'] }} pcs available</p>
                        </div>
                    </div>
                    <p><a href="javascript:void(0);" class="btn btn-black py-3 px-5" onclick="setCartDetail({{ $product[$product_id]['id'] }}, '{{ $product[$product_id]['name'] }}', '{{ $product[$product_id]['display'] }}', {{ $product[$product_id]['price'] }})">Add to Cart</a></p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Products</span>
                    <h2 class="mb-4">Other Products</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($product as $key => $value)
                    @if($value['id'] != $product_id)
                        <div class="col-md-6 col-lg-3 ftco-animate">
                            <div class="product">
                                <a href="#" class="img-prod"><img class="img-fluid" src="{{ asset('images/products/'.$value['display']) }}" alt="Colorlib Template">
                                    <div class="overlay"></div>
                                </a>
                                <div class="text py-3 pb-4 px-3 text-center">
                                    <h3><a href="#">{{ $value['name'] }}</a></h3>
                                    <div class="d-flex">
                                        <div class="pricing">
                                            <p class="price">Rp {{ number_format($value['price']) }}</span></p>
                                        </div>
                                    </div>
                                    <div class="bottom-area d-flex px-3">
                                        <div class="m-auto d-flex">
                                            <a href="{{ route('product_detail', $value['id']) }}" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                                <span><i class="ion-ios-menu"></i></span>
                                            </a>
                                            <a href="javascript:void(0);" class="buy-now d-flex justify-content-center align-items-center mx-1" onclick="setCart({{ $value['id'] }}, '{{ $value['name'] }}', '{{ $value['display'] }}', {{ $value['price'] }})">
                                                <span><i class="ion-ios-cart"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
