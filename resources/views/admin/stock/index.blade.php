@extends('admin.partials.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h2 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}</h2>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.dashboard.index') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route($route['index']) }}">{{ $title }}</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">{{ $page }}</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- basic table -->
            <div class="row">
                <div class="col-12">

                    @if(session()->get('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <i class="fas fa-check-circle"></i> {{ session()->get('success') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-times-circle"></i> {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    <div class="card">
                        <div class="card-body">
                            <h4 class="row">
                                <div class="col-md-6">
                                    <div class="card-title">
                                        {{ $title . " " . $page }}
                                    </div>
                                </div>
{{--                                <div class="col-md-6 text-right">--}}
{{--                                    <div class="card-title">--}}
{{--                                        <a href="{{ route($route['create']) }}"><button type="button" class="btn waves-effect waves-light btn-primary">--}}
{{--                                                <i class="fas fa-plus"> </i> Add New Data--}}
{{--                                            </button></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </h4>
                            <div class="table-responsive">
                                <table id="datalist" class="table table-sm table-striped table-bordered no-wrap">
                                    <thead>
                                    <tr align="center">
                                        <th>#</th>
                                        <th>Product</th>
                                        <th>Category</th>
                                        <th>Stock In</th>
                                        <th>Stock Out</th>
                                        <th>Current Stock</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($data as $value)
                                        <tr>
                                            <td align="center">{{ $i++ }}</td>
                                            <td>{{ $product[$value->product_id]['name'] }}</td>
                                            <td>{{ $category[$product[$value->product_id]['category_id']]['name'] }}</td>
                                            <td>{{ number_format($value->stock_in) }} pcs</td>
                                            <td>{{ number_format($value->stock_out) }} pcs</td>
                                            <td>{{ number_format($value->curr_stock) }} pcs</td>
                                            <td align="center"><span class="badge badge-{{ $value->status == 0 ? 'danger' : 'success' }}">{{ $value->status == 0 ? 'Non Active' : 'Active' }}</span></td>
                                            <td align="center">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route($route['edit'], $value->id) }}">
                                                        <button type="button" class="btn btn-primary">
                                                            <i class="fas fa-tasks"></i> Manage
                                                        </button>
                                                    </a>
{{--                                                    <form class="deleteForm" action="{{ route($route['destroy'], $value->id) }}" row="{{ $value->name }}" method="POST">--}}
{{--                                                        @method('DELETE')--}}
{{--                                                        @csrf--}}
{{--                                                        <button id="btn-form" type="submit" class="btn btn-danger">--}}
{{--                                                            <i class="fas fa-trash-alt"></i> Delete--}}
{{--                                                        </button>--}}
{{--                                                    </form>--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
@endsection
