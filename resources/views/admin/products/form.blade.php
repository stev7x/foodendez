@extends('admin.partials.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h2 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}</h2>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.dashboard.index') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route($route['index']) }}">{{ $title }}</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">{{ $page }}</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- basic table -->
            <div class="row">
                <div class="col-12">

                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-times-circle"></i> {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    <div class="card">
                        <div class="card-body">
                            <h4 class="row">
                                <div class="col-md-6">
                                    <div class="card-title">
                                        Form {{ $page }}
                                    </div>
                                </div>
                            </h4>
                            <form method="POST" action="{{ isset($data['id']) ? route($route['update'], $data['id']) : route($route['store']) }}" enctype="multipart/form-data">

                                @if (isset($data['id']))
                                    @method('PATCH')
                                @endif

                                @csrf

                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="name">Name</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ @$data['name'] }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="category">Category</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <select class="custom-select mr-sm-2" id="category_id" name="category_id">
                                                    <option disabled>Choose...</option>
                                                    @foreach($category as $value)
                                                        <option value="{{ $value['id'] }}" {{ @$data['category_id'] == $value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="name">Display Image</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <img
                                                    src="{{ asset('images/products/'.(isset($data['display']) ? ($data['display'] == '' ? 'Photos-new-icon.png' : @$data['display'] ) : 'Photos-new-icon.png')) }}"
                                                    alt="image" class="img-thumbnail" width="200" height="200" id="preview_img">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="display" name="display" onchange="loadPreview(this);" value="{{ @$data['display'] }}">
                                                    <label class="custom-file-label" for="display">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="price">Price</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="price">Rp.</label>
                                                </div>
                                                <input type="text" class="form-control" name="price" id="price" placeholder="Price" onkeyup="currency('price');" value="{{ isset($data['price']) ? number_format($data['price']) : 0 }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="weight">Weight</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="weight">g</label>
                                                </div>
                                                <input type="number" class="form-control" name="weight" id="weight" placeholder="Weight" value="{{ @$data['weight'] }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="description">Description</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Description">{{ @$data['description'] }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="status">Status</label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_nonactive" value="0" {{ @$data['status'] == 0 ? 'checked' : '' }}>
                                                <label class="form-check-label" for="status_nonactive"><span class="badge badge-danger">Non Active</span></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_active" value="1" {{ @$data['status'] == 1 ? 'checked' : '' }}>
                                                <label class="form-check-label" for="status_active"><span class="badge badge-success">Active</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-info">{{ isset($data['id']) ? 'Update' : 'Create' }}</button>
                                        <button type="reset" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
@endsection
