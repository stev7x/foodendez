</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{ asset('adminmart/assets/libs/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('adminmart/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('adminmart/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!--This Datatable -->
<script src="{{ asset('adminmart/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<!-- apps -->
@if (isset($js))
    <script src="{{ asset('js/' . $js) }}"></script>
@endif
<!-- apps -->
<script src="{{ asset('adminmart/dist/js/app-style-switcher.js') }}"></script>
<script src="{{ asset('adminmart/dist/js/feather.min.js') }}"></script>
<script src="{{ asset('adminmart/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('adminmart/dist/js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('adminmart/dist/js/custom.min.js') }}"></script>
<!--This page JavaScript -->
<script src="{{ asset('adminmart/assets/extra-libs/c3/d3.min.js') }}"></script>
<script src="{{ asset('adminmart/assets/extra-libs/c3/c3.min.js') }}"></script>
{{--<script src="{{ asset('adminmart/assets/libs/chartist/dist/chartist.min.js') }}"></script>--}}
{{--<script src="{{ asset('adminmart/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>--}}
<script src="{{ asset('adminmart/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ asset('adminmart/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js') }}"></script>
{{--<script src="{{ asset('adminmart/dist/js/pages/dashboards/dashboard1.min.js') }}"></script>--}}
</body>

</html>
