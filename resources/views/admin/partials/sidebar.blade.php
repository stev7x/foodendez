<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item {{ (request()->is('dashboard*')) ? 'selected' : '' }}">
                    <a class="sidebar-link sidebar-link" href="{{ route('admin.dashboard.index') }}" aria-expanded="false">
                        <i class="fas fa-home"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item {{ (request()->is('products*')) ? 'selected' : '' }}">
                    <a class="sidebar-link sidebar-link" href="{{ route('admin.products.index') }}" aria-expanded="false">
                        <i class="fas fa-cube"></i><span class="hide-menu">Products</span>
                    </a>
                </li>
                <li class="sidebar-item {{ (request()->is('category*')) ? 'selected' : '' }}">
                    <a class="sidebar-link sidebar-link" href="{{ route('admin.category.index') }}" aria-expanded="false">
                        <i class="fas fa-tags"></i><span class="hide-menu">Category</span>
                    </a>
                </li>
                <li class="sidebar-item {{ (request()->is('stock*')) ? 'selected' : '' }}">
                    <a class="sidebar-link sidebar-link" href="{{ route('admin.stock.index') }}" aria-expanded="false">
                        <i class="fas fa-boxes"></i><span class="hide-menu">Stock</span>
                    </a>
                </li>
                <li class="sidebar-item {{ (request()->is('orders*')) ? 'selected' : '' }}">
                    <a class="sidebar-link sidebar-link" href="{{ route('admin.orders.index') }}" aria-expanded="false">
                        <i class="fas fa-shopping-basket "></i><span class="hide-menu">Orders</span>
                    </a>
                </li>
{{--                <li class="sidebar-item">--}}
{{--                    <a class="sidebar-link sidebar-link" href="index.html" aria-expanded="false">--}}
{{--                        <i class="far fa-building"></i><span class="hide-menu">Bank</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
