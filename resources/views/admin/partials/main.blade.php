<!-- Header -->
@include('admin.partials.header')

<!-- Sidebar -->
@include('admin.partials.sidebar')

<!-- Content -->
@yield('content')

<!-- Footer -->
@include('admin.partials.footer')
