<?php

return [
    'nama' => 'Foodendez',
    'deskripsi' => 'Selalu Terdepan Dan Menjadi Sasaran Konsumen Karena Rasanya Yang Enak Dan Gurih ...  Dan Nambah Nagih .... Krenyesnya Sampai Ke Hati ❤',
    'alamat' => 'Bumi panyawangan real estate jl pinus IV no 15',
    'email' => 'Foodendez@gmail.com',
    'phone' => '+6285795130165',
    'ig' => [
        'name' => 'Foodendez',
        'url' => 'https://www.instagram.com/foodendez/'
    ],
];
