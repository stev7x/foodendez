<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\StockController;
use App\Http\Controllers\Admin\OrderController;

use App\Http\Controllers\App\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::namespace('App')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/product_detail/{id}', [HomeController::class, 'product_detail'])->name('product_detail');
    Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
    Route::get('/about', [HomeController::class, 'about'])->name('about');
    Route::get('/cart', [HomeController::class, 'cart'])->name('cart');
    Route::post('/create_order', [HomeController::class, 'create_order'])->name('create_order');
});
Route::get('/sendemail', 'SendEmailController@index');
Route::post('/sendemail/send', 'SendEmailController@send');

Route::prefix('admin')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('/', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/', 'LoginController@login')->name('admin.login.post');

        Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard.index');

        // Products
        Route::group(['prefix' => 'products'], function () {
            Route::get('/', [ProductController::class, 'index'])->name('admin.products.index');
            Route::get('/create', [ProductController::class, 'create'])->name('admin.products.create');
            Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('admin.products.edit');
            Route::get('/show/{id}', [ProductController::class, 'show'])->name('admin.products.show');

            Route::post('/store', [ProductController::class, 'store'])->name('admin.products.store');
            Route::patch('/update/{id}', [ProductController::class, 'update'])->name('admin.products.update');
            Route::delete('/destroy/{id}', [ProductController::class, 'destroy'])->name('admin.products.destroy');
        });

        // Category
        Route::group(['prefix' => 'category'], function () {
            Route::get('/', [CategoryController::class, 'index'])->name('admin.category.index');
            Route::get('/create', [CategoryController::class, 'create'])->name('admin.category.create');
            Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('admin.category.edit');
            Route::get('/show/{id}', [CategoryController::class, 'show'])->name('admin.category.show');

            Route::post('/store', [CategoryController::class, 'store'])->name('admin.category.store');
            Route::patch('/update/{id}', [CategoryController::class, 'update'])->name('admin.category.update');
            Route::delete('/destroy/{id}', [CategoryController::class, 'destroy'])->name('admin.category.destroy');
        });

        // Stock
        Route::group(['prefix' => 'stock'], function () {
            Route::get('/', [StockController::class, 'index'])->name('admin.stock.index');
            Route::get('/create', [StockController::class, 'create'])->name('admin.stock.create');
            Route::get('/edit/{id}', [StockController::class, 'edit'])->name('admin.stock.edit');
            Route::get('/show/{id}', [StockController::class, 'show'])->name('admin.stock.show');

            Route::post('/store', [StockController::class, 'store'])->name('admin.stock.store');
            Route::patch('/update/{id}', [StockController::class, 'update'])->name('admin.stock.update');
            Route::delete('/destroy/{id}', [StockController::class, 'destroy'])->name('admin.stock.destroy');
        });

        // Orders
        Route::group(['prefix' => 'orders'], function () {
            Route::get('/', [OrderController::class, 'index'])->name('admin.orders.index');
            Route::get('/create', [OrderController::class, 'create'])->name('admin.orders.create');
            Route::get('/edit/{id}', [OrderController::class, 'edit'])->name('admin.orders.edit');
            Route::get('/show/{id}', [OrderController::class, 'show'])->name('admin.orders.show');

            Route::post('/store', [OrderController::class, 'store'])->name('admin.orders.store');
            Route::patch('/update/{id}', [OrderController::class, 'update'])->name('admin.orders.update');
            Route::delete('/destroy/{id}', [OrderController::class, 'destroy'])->name('admin.orders.destroy');
        });
    });
});


