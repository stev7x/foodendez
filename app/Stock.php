<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    // table name
    protected $table = 'stock';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'stock_in',
        'stock_out',
        'curr_stock',
        'status'
    ];
}
