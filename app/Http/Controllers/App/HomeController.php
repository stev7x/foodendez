<?php

namespace App\Http\Controllers\App;

use App\Category;
use App\Orders;
use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    private $product, $category, $stock;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->product = new Product();
        $this->category = new Category();
        $this->stock = new Stock();
    }

    public function index()
    {
        return view('app.home')->with([
            'title' => 'Home',
            'page' => 'Home',
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'js' => 'app/home.js'
        ]);
    }

    public function product_detail($id)
    {
        return view('app.product_detail')->with([
            'title' => 'Product',
            'page' => 'Product',
            'product_id' => $id,
            'category' => $this->category::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'stock' => $this->stock::get()->keyBy('product_id')->toArray(),
            'js' => 'app/product_detail.js'
        ]);
    }

    public function about()
    {
        return view('app.about')->with([
            'title' => 'About',
            'page' => 'About',
            'js' => 'app/about.js'
        ]);
    }

    public function contact()
    {
        return view('app.contact')->with([
            'title' => 'Contact',
            'page' => 'Contact',
            'js' => 'app/contact.js'
        ]);
    }

    public function cart()
    {
        return view('app.cart')->with([
            'title' => 'Cart',
            'page' => 'Cart',
            'js' => 'app/cart.js'
        ]);
    }

    public function create_order(Request $request) {
        $response = false;

        $order = new Orders();
        $product = new Product();
        $stock = new Stock();

        $data_product = $product::get()->keyBy('id')->toArray();
        $data_stock = $stock::get()->keyBy('product_id')->toArray();

        $validator = Validator::make($request->all(), [
            'name'             => 'required',
            'email'            => 'required',
            'phone'            => 'required',
            'pos_code'         => 'required',
            'address'          => 'required',
            'subtotal'         => 'required',
            'delivery'         => 'required',
            'total'            => 'required',
            'product'          => 'required',
            'qty'              => 'required',
            'transaction_code' => 'required'
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $index = 0;
        foreach ($request['product'] as $value) {
            $order->created_by        = @Auth::user()->id ? Auth::user()->id : 0;
            $order->created_at        = date('Y-m-d H:i:s');
            $order->updated_by        = @Auth::user()->id ? Auth::user()->id : 0;
            $order->updated_at        = date('Y-m-d H:i:s');
            $order->name              = $request['name'];
            $order->email             = $request['email'];
            $order->phone             = $request['phone'];
            $order->pos_code          = $request['pos_code'];
            $order->address           = $request['address'];
            $order->harga_satuan      = $data_product[$value]['price'];
            $order->qty               = $request['qty'][$index];
            $order->harga_total       = $request['qty'][$index] * $data_product[$value]['price'];
            $order->diskon            = 0;
            $order->note              = '';
            $order->status_pembayaran = 0;
            $order->status_pengiriman = 0;
            $order->status            = 1;
            $order->product_id        = $value;
            $order->transaction_code  = $request['transaction_code'];
            $index++;

            $response = $order->save();
        }

        $index = 0;
        if ($response) {
            foreach ($request['product'] as $value) {
                $id_stock = $data_stock[$value]['id'];
                $stock = $stock->find($id_stock);

                $stock->updated_by = @Auth::user()->id ? Auth::user()->id : 0;
                $stock->updated_at = date('Y-m-d H:i:s');
                $stock->stock_out  = $stock->stock_out + $request['qty'][$index];
                $stock->curr_stock = $stock->curr_stock - $request['qty'][$index];
                $index++;

                $response = $stock->update();
            }
        }


        if ($response)
        {
            return redirect('/')->with('success', 'Checkout success !');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }
}
