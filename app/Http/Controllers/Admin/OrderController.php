<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Kecamatan;
use App\Kelurahan;
use App\Kota;
use App\Product;
use App\Orders;
use App\Provinsi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    private
        $name = 'Order',
        $title = 'Orders',
        $js = 'admin/orders.js',
        $base_route = 'admin.orders.',
        $route, $date, $model, $user, $product, $provinsi, $kota, $kecamatan, $kelurahan;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->model = new Orders();
        $this->user = new User();
        $this->product = new Product();
        $this->provinsi = new Provinsi();
        $this->kota = new Kota();
        $this->kecamatan = new Kecamatan();
        $this->kelurahan = new Kelurahan();

        $this->date = date('Y-m-d H:i:s');

        $this->route = array(
            'index' => $this->base_route.'index',
            'create' => $this->base_route.'create',
            'edit' => $this->base_route.'edit',
            'show' => $this->base_route.'show',
            'store' => $this->base_route.'store',
            'update' => $this->base_route.'update',
            'destroy' => $this->base_route.'destroy'
        );
    }

    public function index()
    {
        $data = $this->model->all();

        return view($this->base_route.'index')->with([
            'title' => $this->title,
            'page' => 'List',
            'data' => $data,
            'user' => $this->user::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function create()
    {
        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Create',
            'data' => '',
            'user' => $this->user::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'provinsi' => $this->provinsi::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'  => 'required|unique:orders',
            'orders_in'   => 'required',
            'orders_out'  => 'required',
            'curr_orders' => 'required',
            'status'      => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $imageName = '';
        if ($request['display'] != '') $file = $request->file('display');
        if ($request['display'] != '') $imageName = time().'.'.$file->extension();

        $this->model->created_by  = Auth::guard('admin')->user()->id;
        $this->model->created_at  = $this->date;
        $this->model->updated_by  = Auth::guard('admin')->user()->id;
        $this->model->updated_at  = $this->date;
        $this->model->product_id  = $request['product_id'];
        $this->product->display   = $imageName;
        $this->model->orders_in   = $request['orders_in'];
        $this->model->orders_out  = $request['orders_out'];
        $this->model->curr_orders = $request['curr_orders'];
        $this->model->status      = $request['status'];

        if ($request['display'] != '') $file->move(public_path('images/products'), $imageName);

        if ($response = $this->model->save())
        {
            return redirect('/admin/orders')->with('success', $this->name.' has been added');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function show($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function edit($id)
    {
        $data = $this->model->where('id', $id)->first();

        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Manage',
            'data' => $data,
            'user' => $this->user::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status_pembayaran' => 'required',
            'status_pengiriman' => 'required',
            'status'            => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model = $this->model->find($id);

        $this->model->updated_by        = Auth::guard('admin')->user()->id;
        $this->model->updated_at        = $this->date;
        $this->model->status_pembayaran = $request['status_pembayaran'];
        $this->model->status_pengiriman = $request['status_pengiriman'];
        $this->model->status            = $request['status'];

        if ($response = $this->model->save())
        {
            return redirect('/admin/orders')->with('success', $this->name.' has been updated');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function destroy($id)
    {
        $this->model = $this->model->find($id);

        if ($response = $this->model->delete())
        {
            return redirect('/admin/orders')->with('success', $this->name.' has been deleted Successfully');
        }
        else
        {
            return redirect('/admin/orders')->withErrors($response)->withInput();
        }
    }

    public function getKota($id) {
        return $this->kota->where('province_id', $id)->toArray();
    }

    public function getKecamatan($id) {
        return $this->kecamatan->where('regency_id', $id)->toArray();
    }

    public function getKelurahan($id) {
        return $this->kecamatan->where('district_id', $id)->toArray();
    }
}
