<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;

class ProductController extends Controller
{
    private
        $name = 'Product',
        $title = 'Products',
        $js = 'admin/products.js',
        $base_route = 'admin.products.',
        $route, $date, $model, $category, $stock;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->model = new Product();
        $this->category = new Category();
        $this->stock = new Stock();
        $this->date = date('Y-m-d H:i:s');

        $this->route = array(
            'index' => $this->base_route.'index',
            'create' => $this->base_route.'create',
            'edit' => $this->base_route.'edit',
            'show' => $this->base_route.'show',
            'store' => $this->base_route.'store',
            'update' => $this->base_route.'update',
            'destroy' => $this->base_route.'destroy'
        );
    }

    public function index()
    {
        $data = $this->model->all();

        return view($this->base_route.'index')->with([
            'title' => $this->title,
            'page' => 'List',
            'data' => $data,
            'category' => $this->category::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function create()
    {
        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Create',
            'data' => '',
            'category' => $this->category::all(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required|unique:products',
            'display'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price'       => 'required',
            'weight'      => 'required',
            'description' => 'required',
            'status'      => 'required',
            'category_id' => 'required'
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $imageName = '';
        if ($request['display'] != '') $file = $request->file('display');
        if ($request['display'] != '') $imageName = time().'.'.$file->extension();

        $this->model->created_by  = Auth::guard('admin')->user()->id;
        $this->model->created_at  = $this->date;
        $this->model->updated_by  = Auth::guard('admin')->user()->id;
        $this->model->updated_at  = $this->date;
        $this->model->name        = $request['name'];
        $this->model->display     = $imageName;
        $this->model->price       = str_replace(',', '', $request['price']);
        $this->model->weight      = str_replace(',', '', $request['weight']);
        $this->model->description = $request['description'];
        $this->model->status      = $request['status'];
        $this->model->category_id = $request['category_id'];

        if ($request['display'] != '') $file->move(public_path('images/products'), $imageName);

        if ($response = $this->model->save())
        {
            $this->stock->created_by  = Auth::guard('admin')->user()->id;
            $this->stock->created_at  = $this->date;
            $this->stock->updated_by  = Auth::guard('admin')->user()->id;
            $this->stock->updated_at  = $this->date;
            $this->stock->product_id = $this->model->id;
            $this->stock->curr_stock = 0;
            $this->stock->stock_in = 0;
            $this->stock->stock_out = 0;
            $this->stock->status = 0;

            $this->stock->save();

            return redirect('/admin/products')->with('success', $this->name.' has been added');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function show($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function edit($id)
    {
        $data = $this->model->where('id', $id)->first();

        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Edit',
            'data' => $data,
            'category' => $this->category::all(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'display'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price'       => 'required',
            'weight'      => 'required',
            'description' => 'required',
            'status'      => 'required',
            'category_id' => 'required'
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model = $this->model->find($id);

        if ($request['display'] != '') $file = $request->file('display');
        if ($request['display'] != '') $imageName = time().'.'.$file->extension();

        $this->model->updated_by  = Auth::guard('admin')->user()->id;
        $this->model->updated_at  = $this->date;
        $this->model->name        = $request['name'];
        if ($request['display'] != '') $this->model->display = $imageName;
        $this->model->price       = str_replace(',', '', $request['price']);
        $this->model->weight      = str_replace(',', '', $request['weight']);
        $this->model->description = $request['description'];
        $this->model->status      = $request['status'];
        $this->model->category_id = $request['category_id'];

        if ($request['display'] != '') $file->move(public_path('images/products'), $imageName);

        if ($response = $this->model->save())
        {
            return redirect('/admin/products')->with('success', $this->name.' has been updated');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function destroy($id)
    {
        $this->model = $this->model->find($id);

        if ($response = $this->model->delete())
        {
            $stock = $this->stock->where('product_id', $id)->first();
            $stock->delete();

            return redirect('/admin/products')->with('success', $this->name.' has been deleted Successfully');
        }
        else
        {
            return redirect('/admin/products')->withErrors($response)->withInput();
        }
    }
}
