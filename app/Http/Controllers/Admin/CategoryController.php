<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    private
        $name = 'Category',
        $title = 'Category',
        $js = 'admin/category.js',
        $base_route = 'admin.category.',
        $route, $date, $model;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->model = new Category();
        $this->date = date('Y-m-d H:i:s');

        $this->route = array(
            'index' => $this->base_route.'index',
            'create' => $this->base_route.'create',
            'edit' => $this->base_route.'edit',
            'show' => $this->base_route.'show',
            'store' => $this->base_route.'store',
            'update' => $this->base_route.'update',
            'destroy' => $this->base_route.'destroy'
        );
    }

    public function index()
    {
        $data = $this->model->all();

        return view($this->base_route.'index')->with([
            'title' => $this->title,
            'page' => 'List',
            'data' => $data,
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function create()
    {
        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Create',
            'data' => '',
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required|unique:categories',
            'status' => 'required'
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model->created_by = Auth::guard('admin')->user()->id;
        $this->model->created_at = $this->date;
        $this->model->updated_by = Auth::guard('admin')->user()->id;
        $this->model->updated_at = $this->date;
        $this->model->name       = $request['name'];
        $this->model->status     = $request['status'];

        if ($response = $this->model->save())
        {
            return redirect('/admin/category')->with('success', $this->name.' has been added');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function show($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function edit($id)
    {
        $data = $this->model->where('id', $id)->first();

        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Edit',
            'data' => $data,
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|unique:categories',
            'status'           => 'required'
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model = $this->model->find($id);

        $this->model->updated_by = Auth::guard('admin')->user()->id;
        $this->model->updated_at = $this->date;
        $this->model->name       = $request['name'];
        $this->model->status     = $request['status'];

        if ($response = $this->model->save())
        {
            return redirect('/admin/category')->with('success', $this->name.' has been updated');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function destroy($id)
    {
        $this->model = $this->model->find($id);

        if ($response = $this->model->delete())
        {
            return redirect('/admin/category')->with('success', $this->name.' has been deleted Successfully');
        }
        else
        {
            return redirect('/admin/category')->withErrors($response)->withInput();
        }
    }
}
