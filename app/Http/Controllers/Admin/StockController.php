<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StockController extends Controller
{
    private
        $name = 'Stock',
        $title = 'Stock',
        $js = 'admin/stock.js',
        $base_route = 'admin.stock.',
        $route, $date, $model, $product, $category;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->model = new Stock();
        $this->product = new Product();
        $this->category = new Category();
        $this->date = date('Y-m-d H:i:s');

        $this->route = array(
            'index' => $this->base_route.'index',
            'create' => $this->base_route.'create',
            'edit' => $this->base_route.'edit',
            'show' => $this->base_route.'show',
            'store' => $this->base_route.'store',
            'update' => $this->base_route.'update',
            'destroy' => $this->base_route.'destroy'
        );
    }

    public function index()
    {
        $data = $this->model->all();

        return view($this->base_route.'index')->with([
            'title' => $this->title,
            'page' => 'List',
            'data' => $data,
            'category' => $this->category::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function create()
    {
        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Create',
            'data' => '',
            'category' => $this->category::all(),
            'product' => $this->product::all(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|unique:stock',
            'stock_in'   => 'required',
            'stock_out'  => 'required',
            'curr_stock' => 'required',
            'status'     => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model->created_by = Auth::guard('admin')->user()->id;
        $this->model->created_at = $this->date;
        $this->model->updated_by = Auth::guard('admin')->user()->id;
        $this->model->updated_at = $this->date;
        $this->model->product_id = $request['product_id'];
        $this->model->stock_in   = $request['stock_in'];
        $this->model->stock_out  = $request['stock_out'];
        $this->model->curr_stock = $request['curr_stock'];
        $this->model->status     = $request['status'];

        if ($response = $this->model->save())
        {
            return redirect('/admin/stock')->with('success', $this->name.' has been added');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function show($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function edit($id)
    {
        $data = $this->model->where('id', $id)->first();

        return view($this->base_route.'form')->with([
            'title' => $this->title,
            'page' => 'Manage',
            'data' => $data,
            'category' => $this->category::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'route' => $this->route,
            'js' => $this->js
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'stock_in'   => 'required',
            'stock_out'  => 'required',
            'curr_stock' => 'required',
            'status'     => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $this->model = $this->model->find($id);

        $this->model->updated_by = Auth::guard('admin')->user()->id;
        $this->model->updated_at = $this->date;
        $this->model->product_id = $request['product_id'];
        $this->model->stock_in   = $this->model->stock_in + $request['stock_in'];
        $this->model->stock_out  = $this->model->stock_out + $request['stock_out'];
        $this->model->curr_stock = ($request['curr_stock'] + $request['stock_in']) - $request['stock_out'];
        $this->model->status     = $request['status'];

        if ($response = $this->model->save())
        {
            return redirect('/admin/stock')->with('success', $this->name.' has been updated');
        }
        else
        {
            return redirect()->back()->withErrors($response)->withInput();
        }
    }

    public function destroy($id)
    {
        $this->model = $this->model->find($id);

        if ($response = $this->model->delete())
        {
            return redirect('/admin/stock')->with('success', $this->name.' has been deleted Successfully');
        }
        else
        {
            return redirect('/admin/stock')->withErrors($response)->withInput();
        }
    }
}
