<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Orders;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    private
        $title = 'Dashboard',
        $page = 'Products',
        $js = 'admin/dashboard.js',
        $base_route = 'admin.dashboard.',
        $route, $order, $user, $product;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->order = new Orders();
        $this->product = new Product();
        $this->user = new User();

        $this->date = date('Y-m-d H:i:s');

        $this->route = array(
            'index' => $this->base_route.'index',
            'show' => $this->base_route.'show',
            'store' => $this->base_route.'store'
        );
    }

    public function index()
    {
        $data = $this->order->groupBy('product_id')->orderBy('qty','DESC')->get();
        $customer = $this->order->count();
        $earnings = $this->order->groupBy('created_at')->get()->sum('harga_total');

        return view('admin.dashboard.index')->with([
            'title' => $this->title,
            'page' => 'List',
            'data' => $data,
            'customer' => $customer,
            'earnings' => $earnings,
            'user' => $this->user::get()->keyBy('id')->toArray(),
            'product' => $this->product::get()->keyBy('id')->toArray(),
            'page' => $this->page,
            'js' => $this->js
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|unique:orders',
            'orders_in'   => 'required',
            'orders_out'  => 'required',
            'curr_orders' => 'required',
            'status'     => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $imageName = '';
        if ($request['display'] != '') $file = $request->file('display');
        if ($request['display'] != '') $imageName = time().'.'.$file->extension();

        $this->order->created_by = Auth::guard('admin')->user()->id;
        $this->order->created_at = $this->date;
        $this->order->updated_by = Auth::guard('admin')->user()->id;
        $this->order->updated_at = $this->date;
        $this->order->product_id = $request['product_id'];
        $this->product->display     = $imageName;
        $this->order->orders_in   = $request['orders_in'];
        $this->order->orders_out  = $request['orders_out'];
        $this->order->curr_orders = $request['curr_orders'];
        $this->order->status     = $request['status'];

        if ($request['display'] != '') $file->move(public_path('images/products'), $imageName);

        else
        {
            return redirect('/admin/dashboard')->withErrors($response)->withInput();
        }
    }

    public function show($id)
    {
        return $this->order->where('id', $id)->first();
    }

}
