<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    // table name
    protected $table = 'orders';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'address',
        'telephone_number',
        'pos_code',
        'provinsi',
        'kota',
        'kecamatan',
        'kelurahan',
        'harga_satuan',
        'qty',
        'harga_total',
        'note',
        'status_pembayaran',
        'status_pengiriman',
        'status',
        'user_id',
        'product_id'
    ];
}
