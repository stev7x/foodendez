<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    // table name
    protected $table = 'reg_villages';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'district_id',
        'name'
    ];
}
