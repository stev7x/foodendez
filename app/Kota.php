<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    // table name
    protected $table = 'reg_regencies';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'province_id',
        'name'
    ];
}
