<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // table name
    protected $table = 'products';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'display',
        'price',
        'weight',
        'description',
        'status',
        'category_id'
    ];
}
