function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function deleteRow(e, id) {
    let currentCartCounter = parseInt(localStorage.getItem('cartCounter'));
    let currentTotal = parseInt(localStorage.getItem('total'));
    let cartItems = JSON.parse(localStorage.getItem('productsInCart'));

    e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);

    let row = cartItems[id];
    delete cartItems[id];

    localStorage.setItem('productsInCart', JSON.stringify(cartItems));
    localStorage.setItem('cartCounter', currentCartCounter - +row.inCart);
    localStorage.setItem('total', currentTotal - +row.total);

    $('#chart_counter').text(currentCartCounter - +row.inCart);
    $('#subtotal').html('Rp '+formatNumber(currentTotal - +row.total));
    $('#delivery').html('Rp '+formatNumber(0));
    $('#discount').html('Rp '+formatNumber(0));
    $('#total').html('Rp '+formatNumber(currentTotal - +row.total));

    $('#v_subtotal').val(currentTotal - +row.total);
    $('#v_delivery').val(0);
    $('#v_discount').val(0);
    $('#v_total').val(currentTotal - +row.total);
}

function showCartItems() {
    let currentCartCounter = parseInt(localStorage.getItem('cartCounter'));
    let currentTotal = parseInt(localStorage.getItem('total'));
    let cartItems = JSON.parse(localStorage.getItem('productsInCart'));
    let product_list = document.querySelector('#product_list');
    let item_product = document.querySelector('#item_product');
    let asset_url = 'http://127.0.0.1:8000/images/products/';

    if (product_list && cartItems) {
        product_list.innerHTML = '';
        item_product.innerHTML = '';

        Object.values(cartItems).map((item, index) => {
            product_list.innerHTML += '' +
                '<tr class="text-center">' +
                '<td class="product-remove"><a href="javascript:void(0);" onClick="deleteRow(this, '+item.id+')"><span class="ion-ios-close"></span></a></td>' +
                '<td class="image-prod"><div class="img" style="background-image:url('+asset_url+item.display+');"></div></td>' +
                '<td class="product-name">' +
                '<h3>'+item.name+'</h3>' +
                '</td>' +
                '<td class="price">Rp '+formatNumber(item.price)+'</td>' +
                '<td class="quantity">' +
                '<div class="input-group mb-3">\n' +
                '<input type="text" name="quantity" class="quantity form-control input-number" value="'+item.inCart+'" min="1" max="100">' +
                '</div>' +
                '</td>' +
                '<td class="total">Rp '+formatNumber(item.total)+'</td>' +
                '</tr><!-- END TR-->';

            item_product.innerHTML += '' +
                '<input type="hidden" name="product['+index+']" value="'+item.id+'">' +
                '<input type="hidden" name="qty['+index+']" value="'+item.inCart+'">'
        })
    }

    $('#v_subtotal').html('Rp '+formatNumber(currentTotal));
    $('#v_delivery').html('Rp '+formatNumber(0));
    $('#v_discount').html('Rp '+formatNumber(0));
    $('#v_total').html('Rp '+formatNumber(currentTotal));

    $('#subtotal').val(currentTotal);
    $('#delivery').val(0);
    $('#discount').val(0);
    $('#total').val(currentTotal);
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function checkout() {
    let trncd = makeid(32);
    alert("Checkout Success!\nLakukan konfirmasi pembayaran dengan Transfer ke no Rek {{no_rek}} AN {{nama}}\nScreenshot bukti transfer lalu kirimkan bersama kode transaksi via Whatsapp ke no {{n_wa}}\n\nKode Transaksi : " + trncd);

    $('#transaction_code').val(trncd);

    localStorage.setItem('productsInCart', JSON.stringify({}));
    localStorage.setItem('cartCounter', 0);
    localStorage.setItem('total', 0);

    onLoadCheckLocalStorage();
}

showCartItems();
