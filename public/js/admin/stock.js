$('#datalist').DataTable();

$(".deleteForm").on("submit", function(){
    return confirm("Are you sure permanently delete " + this.getAttribute("row") + " ?");
});

// $(".number").keypress(function (e) {
//     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
//         return false;
//     }
// });

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function currency(id) {
    let currentValue = $('#'+id).val().toString().replace(/,/g, '');
    let currentPlus = $('#stock_in').val().toString().replace(/,/g, '');
    let currentMin = $('#stock_out').val().toString().replace(/,/g, '');
    let currentStock = $('#base_stock').val().toString().replace(/,/g, '');

    $('#'+id).val(formatNumber(+currentValue));
    $('#c_stock').text(formatNumber((+currentStock + +currentPlus) - +currentMin));
}
