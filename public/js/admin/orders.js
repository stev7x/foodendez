$('#datalist').DataTable();

$(".deleteForm").on("submit", function(){
    return confirm("Are you sure permanently delete " + this.getAttribute("row") + " ?");
});

// $(".number").keypress(function (e) {
//     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
//         return false;
//     }
// });

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function currency(id) {
    let currentValue = $('#'+id).val().toString().replace(/,/g, '');
    console.log(currentValue);
    $('#'+id).val(formatNumber(+currentValue));
}
